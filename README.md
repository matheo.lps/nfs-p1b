# Need For Speed P1B
## Le jeu CSharp NFS-P1B propose des courses de "street race" et permet par la suite de remporter d'autres voitures au choix.

Membres du projet: Mr. Lopes, Mr. Ioset, Mr. Piaulenne

Déroulement: 
Lors de la première connexion: 
1. Demande du pseudonyme de l'utilisateur et d'un premier véhicule de catégorie BP. 
_(3 catégories: BP -> Basses Performances, PM -> Performances Moyennes, HP -> Hautes Performances)_
2. Affichage du Main-Menu, possibilité de naviguer entre le garage et le QG Street Race.
    - Lors du déplacement dans le QG Street Race, possibilié de lancer une course entre trois niveaux de difficultés : Facile, Difficile, Extrème. 
    _(Le choix de cette dificulté impactera à chaque fois un pourcentage de chance de gagner la course)_
    Une fois la course terminée, les résultats de la course sont affichées, ainsi que l'argent gagné ou perdu. Vous serez ensuite redirié sur le Main-Menu.
    - Lors du déplacement dans le garage, vous serez en possibilité d'améliorer votre voiture, ou d'en acheter de nouvelles.


